#include<iostream>
#include<cstdlib>
#include<iomanip>
#include<ctime>
using namespace std;
void table(int grid[10][10],int row,int column,int level){ //show table
    cout<<"\nLevel: "<<level<<endl;
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){
			cout<<"[";
			if(grid[i][j]==1){
				cout<<"F";
			}else if(grid[i][j]==2){
				cout << "E";
			}else if(grid[i][j]==3){
				cout<<"R";
			}else{
				cout<<" ";
			}
			cout<<"]";
		}
	    cout<<endl;	
	
	}
	
	cout<<endl;
}

void moveFrodo(int grid[10][10],int &row,int &column,bool &ring,int level){//all direction Frodo can move
	int move;
	cout << "Choose number to walk: ";
	cin >> move;
	
	if(move==6){
		grid[row][column]=0;
		column++;
		grid[row][column]=1;		
	}else if(move==4){
		grid[row][column]=0;
		column--;
		grid[row][column]=1;		
	}else if(move==2){
		grid[row][column]=0;
		row++;
		grid[row][column]=1;
		
	}else if(move==8){
		grid[row][column]=0;
		row--;	
		grid[row][column]=1;
		
	}else if(move==3){
		grid[row][column]=0;
		row++;
		column++;
		grid[row][column]=1;
	}else if(move==1){
		grid[row][column]=0;
		row++;
		column--;
		grid[row][column]=1;
	}else if(move==7){
		grid[row][column]=0;
		row--;
		column--;
		grid[row][column]=1;
	}else if(move==9){
		grid[row][column]=0;
		row--;
		column++;
		grid[row][column]=1;
	}else if(move==5){//Frodo can use ring to stop rider move 3 times
		if(ring){
			ring=false;
			for(int i=0;i<3;i++){
				table(grid,row,column,level);
				moveFrodo(grid,row,column,ring,level);
			}
		}
	}	
}

void levelUp(int &level,int grid[10][10],bool &ring){//level up
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){
			grid[i][j]=0;
		}
	}
	
	grid[0][0]=1;
	grid[9][9]=2;
	level++;
	ring=true;
	for(int i=0;i<level*3;i++){//3 riders more for each level
		int r=rand()%10;
		int c=rand()%10;
		grid[r][c]=3;
	}
}

void checkStatus(int &level,int &row,int &column,int grid[10][10],bool &life,bool &ring){//check if Frodo still can move
	if(grid[9][9]==1){
		levelUp(level,grid,ring);
		row=0; 
		column=0;
	}else{
		life=false;
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				if(grid[i][j]==1){
					life=true;
				}
			}
		}
	}
}

void moveRider(int grid[10][10]){//all direction rider can move
	int move;
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){
			if(grid[i][j]==3){
				do{
					move=rand()%9+1;
				}while(move==5);
				
					if(move==6){
						grid[i][j]=0;
		
						grid[i][j+1]=3;		
					}else if(move==4){
						grid[i][j]=0;
				
						grid[i][j-1]=3;		
					}else if(move==2){
						grid[i][j]=0;
					
						grid[i+1][j]=3;
						
					}else if(move==8){
						grid[i][j]=0;
							
						grid[i-1][j]=3;
						
					}else if(move==3){
						grid[i][j]=0;
						
						grid[i+1][j+1]=3;
					}else if(move==1){
						grid[i][j]=0;
						
						grid[i+1][j-1]=3;
					}else if(move==7){
						grid[i][j]=0;
					
						grid[i-1][j-1]=3;
					}else if(move==9){
						grid[i][j]=0;
						
						grid[i-1][j+1]=3;
					}	
								
				
			}
		}
	}
}

int main(){
	srand(time(0));
	cout<<"Frodo & The Lord of the Rings Game"<<endl;
	//move direction of Frodo
	cout<<"  \\     |     /        "<<endl;
	cout<<"   7    8    9"<<endl;
	cout<<"<--4         6-->"<<endl;
	cout<<"   1    2    3"<<endl;
	cout<<"  /     |     \\        "<<endl<<endl;
	
	int grid[10][10]={};
	int move;
	int column=0;
	int row=0;
	bool life=true;
	bool ring=true;
	int level=0;
	levelUp(level,grid,ring);
	while(life){
		table(grid,row,column,level);
		moveFrodo(grid,row,column,ring,level); 
		moveRider(grid);
		checkStatus(level,row,column,grid,life,ring);
		if(level==11){
			break;
		}		
	}
	table(grid,row,column,level);//game result
	if(life){
		cout<<"You Win!";
	}else{
		cout<<"You Lose!";
	}
	
	return 0;
}
